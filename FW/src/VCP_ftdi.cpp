/*
 * VCP_ftdi.cpp
 *
 *  Created on: Jul 16, 2018
 *      Author: vlad
 */

#include "VCP_ftdi.h"
#include "stm32f10x_gpio.h"

#define VIRTUAL_COM_PORT_SIZ_DEVICE_DESC        18
#define VIRTUAL_COM_PORT_SIZ_CONFIG_DESC        67
#define VIRTUAL_COM_PORT_SIZ_STRING_LANGID      4
#define VIRTUAL_COM_PORT_SIZ_STRING_VENDOR      38
#define VIRTUAL_COM_PORT_SIZ_STRING_PRODUCT     50
#define VIRTUAL_COM_PORT_SIZ_STRING_SERIAL      26

#define VCP_EP0_SIZE                            64
#define VCP_VID                                 0x0403
#define VCP_PID                                 0x6001
#define VCP_CONFIG_NUM                          1

#define VCP_VENDOR                              u"FTDI"
#define VCP_PRODUCT                             u"FT232R USB UART"
#define VCP_SERIAL                              u"BADF000D"
/*
    'I', 0, 'R', 0, 'D', 0, 'A', 0, ' ', 0, 'V', 0, 'i', 0, 'r', 0,
    't', 0, 'u', 0, 'a', 0, 'l', 0, ' ', 0, 'U', 0, 'S', 0, 'A', 0,
    'R', 0, 'T', 0, ' ', 0, 'P', 0, 'o', 0, 'r', 0, 't', 0, ' ', 0
 * */

#define VIRTUAL_COM_PORT_DATA_SIZE              64
#define VIRTUAL_COM_PORT_INT_SIZE               8

const USB_Device::DeviceDescriptor VCP_DeviceDescriptor =
    {
        bLength            : sizeof( USB_Device::DeviceDescriptor ),
        bDescriptorType    : USB_Device::DescriptorType::Device,
        body               : {
            bcdUSB             : 0x0200,  // USB 2.00
            bDeviceClass       : 0x00,    // CDC
            bDeviceSubClass    : 0x00,
            bDeviceProtocol    : 0x00,
            bMaxPacketSize0    : VCP_EP0_SIZE,
            idVendor           : VCP_VID,
            idProduct          : VCP_PID,
            bcdDevice          : 0x0600,
            bStrDescIdx        : { 1, 2, 3 },
            bNumConfigurations : VCP_CONFIG_NUM,
        }
    };

// Declaration of custom Descriptors
typedef USB_Device::AbstractDescriptor<uint8_t[2]> CS4Descriptor;
typedef USB_Device::AbstractDescriptor<uint8_t[3]> CS5Descriptor;
typedef USB_Device::AbstractDescriptor<uint16_t> LangIDDescriptor;

struct ConfigDescriptor
{
    USB_Device::ConfigurationDescriptor configuration;
    USB_Device::InterfaceDescriptor         interface0;
    USB_Device::EndpointDescriptor              ep1;
    USB_Device::EndpointDescriptor              ep2;
};

const ConfigDescriptor VCP_ConfigDescriptor =
{
        /*Configuration Descriptor*/
    configuration  : {
        bLength         : sizeof( USB_Device::ConfigurationDescriptor ),
        bDescriptorType : USB_Device::DescriptorType::Configuration,
        body            : {
            wTotalLength        : sizeof( ConfigDescriptor ),           // Should be 32
            bNumInterfaces      : 0x01,                        // 1 interface
            bConfigurationValue : 0x01,
            iConfiguration      : 0x00,                        // Index of string descriptor describing the configuration
            bmAttributes        : 0xA0,                        // bus powered
            MaxPower            : 0x2D,                        //  50 mA
        }
    },
    interface0 : {
        bLength         : sizeof( USB_Device::InterfaceDescriptor ),
        bDescriptorType : USB_Device::DescriptorType::Interface,
        body : {
            // Interface descriptor type
            bInterfaceNumber   :  0x00,   // Number of Interface
            bAlternateSetting  :  0x00,   // Alternate setting
            bNumEndpoints      :  0x02,   // One endpoints used
            bInterfaceClass    :  0xFF,   // Vendor specific
            bInterfaceSubClass :  0xFF,   // Vendor specific
            bInterfaceProtocol :  0xFF,   // Vendor specific
            bStrIdxInterface   :  0x02,   // iInterface
        }
    },
        ep1 : {
            /*Endpoint 1 Descriptor*/
            bLength         : sizeof( USB_Device::EndpointDescriptor ),
            bDescriptorType : USB_Device::DescriptorType::Endpoint,
            body : {
                bEndpointAddress : 0x81,   // bEndpointAddress: (IN1)
                bmAttributes     : 0x02,   // bmAttributes: Bulk
                wMaxPacketSize   : VIRTUAL_COM_PORT_DATA_SIZE,
                bInterval        : 0x00,   // ignore for Bulk transfer:
            }
        },
        ep2 : {
            /*Endpoint 2 Descriptor*/
            bLength         : sizeof( USB_Device::EndpointDescriptor ),
            bDescriptorType : USB_Device::DescriptorType::Endpoint,
            body : {
                bEndpointAddress : 0x02,   // bEndpointAddress: (OUT2)
                bmAttributes     : 0x02,   // bmAttributes: Bulk
                wMaxPacketSize   : VIRTUAL_COM_PORT_DATA_SIZE,
                bInterval        : 0x00,   // ignore for Bulk transfer
            }
        }
  };

/* USB String Descriptors */
const LangIDDescriptor VCP_StringLangID =
{
    bLength         : sizeof( LangIDDescriptor ),
    bDescriptorType : USB_Device::DescriptorType::String,
    body            : 0x0409,   // LangID = 0x0409: U.S. English
};

typedef USB_Device::AbstractDescriptor<char16_t[ sizeof( VCP_VENDOR ) /2  ]>  VendorStrDescriptor;
typedef USB_Device::AbstractDescriptor<char16_t[ sizeof( VCP_PRODUCT )/2  ]> ProductStrDescriptor;
typedef USB_Device::AbstractDescriptor<char16_t[ sizeof( VCP_SERIAL ) /2  ]>  SerialStrDescriptor;

const VendorStrDescriptor VCP_StringVendor =
    {
        bLength         : sizeof( VendorStrDescriptor ),
        bDescriptorType : USB_Device::DescriptorType::String,
        body            : VCP_VENDOR,
};

const ProductStrDescriptor VCP_StringProduct =
    {
        bLength         : sizeof( ProductStrDescriptor ),
        bDescriptorType : USB_Device::DescriptorType::String,
        body            : VCP_PRODUCT,
};

const SerialStrDescriptor VCP_StringSerial =
    {
        bLength         : sizeof( SerialStrDescriptor ),
        bDescriptorType : USB_Device::DescriptorType::String,
        body            : VCP_SERIAL,
};

uint8_t _inData[128];
uint8_t _outData[128];
uint8_t RX_Data[64];


VCP_FTDI* VCP_FTDI::__init__()
{
    static VCP_FTDI inst;
    return (VCP_FTDI*)_dev;
}

VCP_FTDI* VCP_FTDI::dev()
{
    return (VCP_FTDI*)_dev;
}

VCP_FTDI::VCP_FTDI() : receiveBuffer( _inData, sizeof(_inData )), waitingBuffer( _outData, sizeof( _outData ))
{
    desc[0].Descriptor      = (uint8_t*)&VCP_DeviceDescriptor;
    desc[0].Descriptor_Size = sizeof(VCP_DeviceDescriptor);
    desc[1].Descriptor      = (uint8_t*)&VCP_ConfigDescriptor;
    desc[1].Descriptor_Size = sizeof(VCP_ConfigDescriptor);
    desc[2].Descriptor      = (uint8_t*)&VCP_StringLangID;
    desc[2].Descriptor_Size = sizeof(VCP_StringLangID);
    desc[3].Descriptor      = (uint8_t*)&VCP_StringVendor;
    desc[3].Descriptor_Size = sizeof(VCP_StringVendor);
    desc[4].Descriptor      = (uint8_t*)&VCP_StringProduct;
    desc[4].Descriptor_Size = sizeof(VCP_StringProduct);
    desc[5].Descriptor      = (uint8_t*)&VCP_StringSerial;
    desc[5].Descriptor_Size = sizeof(VCP_StringSerial);

    pEpInt_IN[0]  = VCP_FTDI::onIn;
    pEpInt_OUT[1] = VCP_FTDI::onOut;

    mode = VCP_FTDI::ConnectMode::Reset;

    evtChar   = 0x0D;
    Dtr_Rts   = 0;
    latencyTO = 16;

    UART_Setup.bitrate    = 115200;
    UART_Setup.databits   = 8;
    UART_Setup.paritytype = 0;
    UART_Setup.stopbits   = 1;

    packetSent     = 0;
    receivedLength = 0;
}

void VCP_FTDI::epSetup()
{

    /* Initialize Endpoint 1 */
    Endpoint::epSetType( Endpoint::Num::EP1, EP_Type_e::Bulk );
    Endpoint::epSetTxAddr( Endpoint::Num::EP1, ENDP1_TXADDR );
    Endpoint::epSetStatus( Endpoint::Num::EP1, EP_Status_e::Disabled, EP_Status_e::NAK );

    /* Initialize Endpoint 3 */
    Endpoint::epSetType( Endpoint::Num::EP2, EP_Type_e::Bulk );
    Endpoint::epSetRxAddr( Endpoint::Num::EP2, ENDP2_RXADDR );
    Endpoint::epSetRxCount(Endpoint::Num::EP2, VIRTUAL_COM_PORT_DATA_SIZE );
    Endpoint::epSetStatus( Endpoint::Num::EP2, EP_Status_e::Valid, EP_Status_e::Disabled );

}

void VCP_FTDI::setConnectionMode( VCP_FTDI::ConnectMode p_mode )
{
    if ( p_mode != dev()->mode )
    {

    }

    dev()->mode = p_mode;

    if ( usbIsConnected() )
        packetSent = 0;
}

void VCP_FTDI::pullSetup()
{
    GPIO_InitTypeDef  GPIO_InitStructure;

//    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE ); //  AFIO_MAPR_SWJ_CFG_JTAGDISABLE

#if defined(STM32F30X)
#error This code is subject to check!!!!

    RCC_AHBPeriphClockCmd(USB_DISC_RCC, ENABLE);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_12;  // Whatever you have :)
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
#else
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
#endif
    GPIO_Init(GPIOA, &GPIO_InitStructure);

}

void VCP_FTDI::pullSet()
{
    GPIO_SetBits(GPIOA, GPIO_Pin_8);

}

void VCP_FTDI::pullClear()
{
    GPIO_ResetBits(GPIOA, GPIO_Pin_8);
}

void VCP_FTDI::onStatus_In()
{
//    if ( dev()->dev_request == VCP_FTDI::RequestSubType::GET_LATENCY_TIMER)
//    {
//        dev()->dev_request = VCP_FTDI::RequestSubType::RESET;
//    }
}

RESULT VCP_FTDI::onDataSetup( uint8_t RequestNo )
{
    uint8_t   *(*CopyRoutine)(uint16_t Length);

    setConnectionMode(VCP_FTDI::ConnectMode::Data);

    dev()->dev_request = VCP_FTDI::RequestSubType( RequestNo );

    if (dev()->Request == USB_Device::RequestType::Vendor_Req)
    {
        switch ( dev()->dev_request )
        {
            case VCP_FTDI::RequestSubType::GET_LATENCY_TIMER:
                CopyRoutine = VCP_FTDI::transfer_GetLatency;
                break;
            case VCP_FTDI::RequestSubType::GET_MODEM_STATUS:
                CopyRoutine = VCP_FTDI::transfer_GetLatency;
                break;
            default:
                return USB_UNSUPPORT;
        }
    }
    else
        return  USB_UNSUPPORT;

    pInformation->Ctrl_Info.CopyData = CopyRoutine;
    pInformation->Ctrl_Info.Usb_wOffset = 0;
    (*CopyRoutine)(0);
    return USB_SUCCESS;
}

RESULT VCP_FTDI::onNoDataSetup( uint8_t RequestNo )
{
    setConnectionMode( VCP_FTDI::ConnectMode::NoData);

    dev()->dev_request = VCP_FTDI::RequestSubType( RequestNo );

    if (dev()->Request == USB_Device::RequestType::Vendor_Req)
    {
        switch ( dev()->dev_request )
        {
            case VCP_FTDI::RequestSubType::SET_LATENCY_TIMER:
                FTDI_SetLatency();
                return USB_SUCCESS;
            case VCP_FTDI::RequestSubType::SET_DATA:
                FTDI_SetData();
                return USB_SUCCESS;
            case VCP_FTDI::RequestSubType::SET_FLOW_CTRL:
                FTDI_SetFlowCtrl();
                return USB_SUCCESS;
            case VCP_FTDI::RequestSubType::SET_BAUD_RATE:
                FTDI_SetBaud();
                return USB_SUCCESS;
            case VCP_FTDI::RequestSubType::SET_EVENT_CHAR:
                //FTDI_SetEvtChar();
                return USB_SUCCESS;
            case VCP_FTDI::RequestSubType::SET_ERROR_CHAR:
                //FTDI_SetErrChar();
                return USB_SUCCESS;
            case VCP_FTDI::RequestSubType::MODEM_CTRL:
                FTDI_ModemCtrl();
                return USB_SUCCESS;
            case VCP_FTDI::RequestSubType::RESET:
                FTDI_Reset();
                return USB_SUCCESS;
            default:
                return USB_UNSUPPORT;
        }
    }
    return USB_UNSUPPORT;
}

uint8_t* VCP_FTDI::transfer_GetLatency(uint16_t Length)
{
    if (Length == 0)
    {
        pInformation->Ctrl_Info.Usb_wLength = 1;
        return 0;
    }
    return (uint8_t *)&( dev()->latencyTO );
}

uint8_t* VCP_FTDI::transfer_GetModemStatus(uint16_t Length)
{
    if (Length == 0)
    {
        pInformation->Ctrl_Info.Usb_wLength = 1;
        return 0;
    }
    return (uint8_t *)&( dev()->Dtr_Rts );
}

void VCP_FTDI::checkLockedData()
{
    if ( dev()->receivedLength && dev()->receiveBuffer.free() >= dev()->receivedLength )
    {
        for( uint8_t i = 0; i < dev()->receivedLength; i++)
            dev()->receiveBuffer.Push( RX_Data[i] );

        receivedLength = 0;
        Endpoint::epSetRxCount(Endpoint::Num::EP2, VIRTUAL_COM_PORT_DATA_SIZE );
        Endpoint::epSetRxStatus( Endpoint::Num::EP2, EP_Status_e::Valid );
    }
}

void VCP_FTDI::pushDataOut()
{
    if ( dev()->packetSent == 0 && dev()->waitingBuffer.isEmpty() == 0 )
    {
        uint8_t TX_Data[64];
        uint8_t i;

        TxStatus* header = (TxStatus*)(TX_Data);

        header->raw        = 0;
        header->field.res  = 0x1;
        header->field.CTS  = 0x1;

        header->field.DR   = 1;
        if ( receiveBuffer.isEmpty() )
            header->field.TEMT = 1;

        for( i = 2; ( i < 64 && dev()->waitingBuffer.isEmpty() == 0 ); i++ )
            TX_Data[ i ] = dev()->waitingBuffer.Pop();

        UserToPMABufferCopy( TX_Data, ENDP1_TXADDR, i );

        Endpoint::epSetTxCount(Endpoint::Num::EP1, i );
        dev()->packetSent += i;
        Endpoint::epSetTxStatus( Endpoint::Num::EP1, EP_Status_e::Valid );
    }
}

void VCP_FTDI::onIn()
{
    dev()->packetSent = 0;
    dev()->pushDataOut();
}

void VCP_FTDI::onOut()
{
    static uint8_t offset = 0;

    dev()->receivedLength = Endpoint::epGetRxCount( Endpoint::Num::EP2 );
    PMAToUserBufferCopy((unsigned char*)RX_Data, ENDP2_RXADDR, dev()->receivedLength);

    dev()->checkLockedData();
}

VCP_FTDI::RequestSubType VCP_FTDI::dev_request;
