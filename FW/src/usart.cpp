/*
 *  usart.c
 *
 *  Created on: Jun 26, 2013
 *      Author: Denis aka caat
 */
extern "C"
{
    #include "stm32f10x_gpio.h"
    #include "stm32f10x_usart.h"
    #include "utils.h"
    #include "ringbuffer.h"
}
#include "usart.h"

static tRingBuffer RingBufferUART2TX;
static tRingBuffer RingBufferUART2RX;
unsigned int IrqCntUart2;

void InitUartBuffer(void);

void InitUartBufferIRQ(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Configure the NVIC Preemption Priority Bits */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

    /* Enable the USARTy Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1; //Preemption Priority
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void UART_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    USART_ClockInitTypeDef USART_ClockInitStructure;

    //Set USART2 Tx (PA2) as AF push-pull
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    //Set USART2 Rx (PA3) as input pull down
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_Init(GPIOA, &GPIO_InitStructure);


    USART_ClockStructInit(&USART_ClockInitStructure);
    USART_ClockInit(USART2, &USART_ClockInitStructure);
    //USART_InitStructure.USART_BaudRate            = 57600;
    USART_InitStructure.USART_BaudRate            = 115200;
    USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits            = USART_StopBits_1;
    USART_InitStructure.USART_Parity              = USART_Parity_No ;
    USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

    USART_Init(USART2, &USART_InitStructure);

    //Enable UART2 Receive interrupt
    USART_ITConfig(      USART2, USART_IT_RXNE, ENABLE);

    USART_IrDACmd(       USART2, ENABLE );
    USART_LINCmd(        USART2, DISABLE );
    USART_HalfDuplexCmd( USART2, DISABLE );
    USART_SetPrescaler(  USART2, 0x01 );

    //Enable UART2
    USART_Cmd(USART2, ENABLE);

    InitUartBuffer();
    InitUartBufferIRQ();
}

int UART_GetChar(void)
{
    return RingBufferGet(&RingBufferUART2RX);
}

int UART_Peek(void)
{
    return RingBufferPeek(&RingBufferUART2RX);
}

int UART_Available(void)
{
    return RingBufferFillLevel(&RingBufferUART2RX);
}

void UART_Flush(void)
{
    while (RingBufferFillLevel(&RingBufferUART2TX) != 0)
        ;
}

void UART_PutCharDirect(uint8_t ch)
{
    while (!(USART2->SR & USART_SR_TXE));

    USART2->DR = ch;
}

void UART_PutChar(uint8_t ch)
{
    //while (!(UART4->SR & USART_SR_TXE));
    //  UART4->DR = ch;
    RingBufferPut(&RingBufferUART2TX, ch, 1);
}

void UART_PutStringDirect(uint8_t *str)
{
    while (*str != 0)
    {
        UART_PutCharDirect(*str);
        str++;
    }
}

void UART_PutString(uint8_t *str)
{
    while (*str != 0)
    {
        UART_PutChar(*str);
        str++;
    }
}

void UART2EnableTxInterrupt(void)
{
    USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
}

void USART2_IRQHandler(void) //UART2 Interrupt handler implementation
{
    int sr = USART2->SR;
    IrqCntUart2++;

    if (sr & USART_FLAG_TXE)
    {
        tRingBuffer *rb = &RingBufferUART2TX;

        if (rb->Read != rb->Write)
        {
            USART2->DR = rb->Buffer[rb->Read];

            if (rb->Read + 1 == RingBufferSize(rb))
            {
                rb->Read = 0;
            }
            else
            {
                rb->Read++;
            }
        }
        else
        {
            USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
            asm volatile("nop");
            asm volatile("nop");
        }
    }

    if (sr & USART_FLAG_RXNE)
    {
        tRingBuffer *rb = &RingBufferUART2RX;

        unsigned char c = USART2->DR;

        if (RingBufferFillLevel(rb) + 1 == RingBufferSize(rb))
        {
            rb->Overrun++;
            return;
        }

        rb->Buffer[rb->Write] = c;

        if (rb->Write + 1 == RingBufferSize(rb))
        {
            rb->Write = 0;
        }
        else
        {
            rb->Write++;
        }
    }
}

void InitUartBuffer(void)
{
    RingBufferInit(&RingBufferUART2TX, &UART2EnableTxInterrupt);
    RingBufferInit(&RingBufferUART2RX, 0L);
}
