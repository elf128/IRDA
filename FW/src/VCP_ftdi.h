/*
 * VCP_ftdi.h
 *
 *  Created on: Jul 16, 2018
 *      Author: vlad
 */

#ifndef SRC_VCP_SRC_VCP_FTDI_H_
#define SRC_VCP_SRC_VCP_FTDI_H_

#include "USB_Device.h"
#include "Buffer.h"
extern "C"
{

    #include "usb_init.h"
    #include "usb_mem.h"
}

class VCP_FTDI: public USB_Device
{
public:
    enum class ConnectMode
    {
        Reset,
        Data,
        NoData
    };

    enum class RequestSubType
    {
        RESET             = 0x00,
        MODEM_CTRL        = 0x01,
        SET_FLOW_CTRL     = 0x02,
        SET_BAUD_RATE     = 0x03,
        SET_DATA          = 0x04,
        GET_MODEM_STATUS  = 0x05,
        SET_EVENT_CHAR    = 0x06,
        SET_ERROR_CHAR    = 0x07,
        SET_LATENCY_TIMER = 0x09,
        GET_LATENCY_TIMER = 0x0A,
    };

    struct LINE_CODING
    {
      uint32_t bitrate;
      uint8_t  databits;
      uint8_t  stopbits;
      uint8_t  paritytype;
    };

    union TxStatus
    {
        struct {
            // Byte 1
            uint16_t res   : 4;  // SHOULD BE 0x1
            uint16_t CTS   : 1;  // Clear To Send
            uint16_t DSR   : 1;  // Data Set Ready
            uint16_t RI    : 1;  // Ring Indicator
            uint16_t RLSD  : 1;  // Receive Line Signal Detect
            // Byte 1
            uint16_t DR    : 1;  //  B0   Data Ready
            uint16_t OE    : 1;  //  B1   Overrun Error
            uint16_t PE    : 1;  //  B2   Parity Error
            uint16_t FE    : 1;  //  B3   Framing Error
            uint16_t BI    : 1;  //  B4   Break Interrupt
            uint16_t THRE  : 1;  //  B5   Transmitter Holding Register
            uint16_t TEMT  : 1;  //  B6   Transmitter Empty
            uint16_t err   : 1;  //  B7   Error in RCVR FIFO
        } field;
        uint16_t raw;
    };

    static RequestSubType dev_request;
    uint32_t              packetSent;
    ByteBuffer<uint8_t>   receiveBuffer;
    ByteBuffer<uint8_t>   waitingBuffer;
    uint32_t              receivedLength;

    uint8_t               evtChar;
    uint8_t               Dtr_Rts;
    uint16_t              latencyTO;

    LINE_CODING           UART_Setup;

    // This is the function you have to call to initialise singleton
    static  VCP_FTDI* dev();
    static  VCP_FTDI* __init__();

    static uint8_t* transfer_GetModemStatus(uint16_t Length);
    static uint8_t* transfer_GetLatency(    uint16_t Length);

    void   checkLockedData();
    void   pushDataOut();
protected:
    ConnectMode mode;

    VCP_FTDI();
    ~VCP_FTDI(){}

    virtual void    pullSetup();
    virtual void    pullSet();
    virtual void    pullClear();

    virtual void    epSetup();

    virtual void    onStatus_In();
    virtual RESULT  onDataSetup( uint8_t RequestNo );
    virtual RESULT  onNoDataSetup( uint8_t RequestNo );

    void    setConnectionMode( VCP_FTDI::ConnectMode p_mode );

    uint8_t usbIsConfigured() { return (devState == USB_Device::State::Configured ); }
    uint8_t usbIsConnected()  { return (mode == VCP_FTDI::ConnectMode::Data);  }

    inline  void FTDI_SetLatency()  { latencyTO = pInformation->USBwValues.bw.bb0; }
    inline  void FTDI_SetData()     { UART_Setup.databits   = pInformation->USBwValues.bw.bb0;
                                      UART_Setup.paritytype = pInformation->USBwValues.bw.bb1 & 0x07;
                                      UART_Setup.stopbits   = (( pInformation->USBwValues.bw.bb1 >> 3 ) & 0x07) + 1;}
    inline  void FTDI_SetFlowCtrl() { Dtr_Rts              |= pInformation->USBwValues.bw.bb0<<4; }
    inline  void FTDI_SetBaud()     { uint16_t DIV = (pInformation->USBwValues.bw.bb1) & 0x3F;
                                      DIV <<= 8;
                                      DIV |=  pInformation->USBwValues.bw.bb0;
                                      DIV <<= 4;
                                      if (pInformation->USBwValues.bw.bb1 & 0xC0 == 0x0C )
                                          DIV |=  0x02;
                                      else
                                          if (pInformation->USBwValues.bw.bb1 & 0x80 )
                                              DIV |=  0x04;
                                          else
                                              if (pInformation->USBwValues.bw.bb1 & 0x40 )
                                                  DIV |=  0x08;
                                      UART_Setup.bitrate    = 48000000/DIV; }

    inline  void FTDI_SetEvtChar()  { latencyTO = pInformation->USBwValues.bw.bb0; }
    inline  void FTDI_SetErrChar()  { latencyTO = pInformation->USBwValues.bw.bb0; }

    inline  void FTDI_ModemCtrl()   {
        Dtr_Rts  = pInformation->USBwValues.bw.bb0 & 0x03; }
    inline  void FTDI_Reset()       { switch (pInformation->USBwValues.bw.bb0)
                                      {
                                          case 2: waitingBuffer.Reset(); break;
                                          case 1: receiveBuffer.Reset(); break;
                                          default:waitingBuffer.Reset();receiveBuffer.Reset(); evtChar = 0x0D; Dtr_Rts = 0;
                                      }}

    static  void onIn();
    static  void onOut();

};

#endif /* SRC_VCP_SRC_VCP_FTDI_H_ */
