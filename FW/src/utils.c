/*
 *  utils.c
 *
 *  Created on: Jun 25, 2013
 *      Author: Denis caat
 */

//#include <math.h>
#include "utils.h"
#include "stm32f10x_rcc.h"
//#include "comio.h"
#include "systick.h"
#include "utils.h"

void readLEDon(void)
{
    GPIO_SetBits(READ_LED_PORT, READ_LED_PIN); //LED on
}

void readLEDoff(void)
{
    GPIO_ResetBits(READ_LED_PORT, READ_LED_PIN); //LED off
}

void readLEDtoggle(void)
{
    __disable_irq();
    GPIO_ToggleBits(READ_LED_PORT, READ_LED_PIN);
    __enable_irq();
}

void writeLEDon(void)
{
    GPIO_SetBits(WRITE_LED_PORT, WRITE_LED_PIN); //LED on
}

void writeLEDoff(void)
{
    GPIO_ResetBits(WRITE_LED_PORT, WRITE_LED_PIN); //LED off
}

void writeLEDtoggle(void)
{
    __disable_irq();
    GPIO_ToggleBits(WRITE_LED_PORT, WRITE_LED_PIN);
    __enable_irq();
}

void Blink(void)
{
//    DEBUG_PutChar('B');

	readLEDon();            //blinking led
    Delay_ms(200);
    readLEDoff();
    Delay_ms(200);
}

void Delay_us(unsigned int us)
{
	int32_t time = micros()+us;
	while( micros() < time );
}

void Delay_ms(unsigned int ms)
{
	int32_t time = millis()+ms;
	while( millis() < time );
}
