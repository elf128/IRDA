/*
 * Buffer.h
 *
 *  Created on: Sep 22, 2015
 *      Author: vadamenko
 */

#ifndef BUFFER_H_
#define BUFFER_H_

#include <cstdint>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"
#pragma GCC diagnostic ignored "-Waggregate-return"

template <typename COUNTER, typename DATATYPE>
class Buffer
{
public:
    void    Reset()  volatile  { this->_headIdx = this->_tailIdx = 0; }
    uint8_t isEmpty() volatile { return ( this->_headIdx == this->_tailIdx ); }
    uint8_t isFull()  volatile { return ( this->NextIdx( this->_headIdx ) == this->_tailIdx ); }
    COUNTER used() volatile    {
        if ( this->_headIdx >= this->_tailIdx ) return this->_headIdx - this->_tailIdx;
        else return this->_headIdx - this->_tailIdx + this->_size; }

    COUNTER free() volatile    { return this->_size - this->used(); }

    Buffer( DATATYPE *inbuffer, COUNTER Size )
    {
        this->_buffer  = inbuffer;
        this->_size    = Size;
        this->_headIdx = this->_tailIdx = 0;
    }

    void Push(const DATATYPE& data) volatile
    {
        this->_buffer[ this->_headIdx ] = data;
        this->_headIdx = this->NextIdx( this->_headIdx );
    }

    DATATYPE Pop() volatile
    {
        DATATYPE D = this->_buffer[ this->_tailIdx ];
        this->_tailIdx = this->NextIdx( this->_tailIdx );
        return D;
    }

    void Pop(DATATYPE& outData) volatile
    {
        outData = this->_buffer[ this->_tailIdx ];
        this->_tailIdx = this->NextIdx( this->_tailIdx );
    }

    DATATYPE Get() volatile
    {
        DATATYPE D = this->_buffer[ this->_tailIdx ];
        return D;
    }

protected:
	DATATYPE* _buffer;
    COUNTER   _size;
    COUNTER   _headIdx;
    COUNTER   _tailIdx;
    COUNTER __inline__ NextIdx(COUNTER idx) volatile { COUNTER N = ( idx + 1 ); return (N == this->_size )?0:N; }
//    COUNTER __inline__ SumIdx(COUNTER idx, COUNTER add) volatile { COUNTER N = ( idx + add ); return (N > this->_size )?( N - this->size ):N; }
};
#pragma GCC diagnostic pop

template <typename COUNTER>
class ByteBuffer : public Buffer<COUNTER, uint8_t >
{
public:
    ByteBuffer( uint8_t* buffer, COUNTER Size ) : Buffer<COUNTER, uint8_t >( buffer, Size) {}

    uint8_t Try(uint8_t *data, uint8_t size)
    {
        COUNTER T = this->TailIdx;
        for(uint8_t i=0;i<size;i++)
        {
            if ( T == this->HeadIdx )
                return i;
            if ( (data[i] != '*') && (this->buffer[ T ] != data[ i ]) )
                return i;
            T = this->NextIdx( T );
        }
        this->TailIdx = T;
        return size;
    }
};

#endif /* BUFFER_H_ */
