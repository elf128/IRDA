/*
 * USB_Device.cpp
 *
 *  Created on: Jul 25, 2018
 *      Author: vlad
 */

#include "USB_Device.h"
#include "USB_hal_stm32f1x.h"

extern "C"
{
    #include "usb_sil.h"
    #include "usb_init.h"
//    #include "usb_regs.h"
}

void USB_Device::dev_Init()
{
    //Get_SerialNum();

    pInformation->Current_Configuration = 0;

    /* Connect the device */
    dev()->PowerOn();

    /* Perform basic device initialization operations */
    USB_SIL_Init();

    dev()->devState = USB_Device::State::Unconnected;
}

void USB_Device::dev_Reset()
{
    //vcpBootBlockTime = millis();

    /* Set Virtual_Com_Port DEVICE as not configured */
    pInformation->Current_Configuration = 0;

    /* Current Feature initialization */
    pInformation->Current_Feature = desc[1].Descriptor[7];

    /* Set Virtual_Com_Port DEVICE with the default Interface*/
    pInformation->Current_Interface = 0;

    setBTable(BTABLE_ADDRESS); // stm32 specific.

    /* Initialize Endpoint 0 */
    Endpoint::epSetType(     Endpoint::Num::EP0, EP_Type_e::Ctrl );
    Endpoint::epSetTxStatus( Endpoint::Num::EP0, EP_Status_e::Stall );

    Endpoint::epSetRxAddr(   Endpoint::Num::EP0, ENDP0_RXADDR );
    Endpoint::epSetTxAddr(   Endpoint::Num::EP0, ENDP0_TXADDR );

    //Clear_Status_Out(ENDP0);
    Endpoint::epClrOutStatus(Endpoint::Num::EP0 );
    Endpoint::epSetRxCount(  Endpoint::Num::EP0, Device_Property.MaxPacketSize );
    Endpoint::epSetRxStatus( Endpoint::Num::EP0, EP_Status_e::Valid );

    dev()->epSetup();

    /* Set this device to response on default address */
    SetDeviceAddress(0);

    dev()->devState = USB_Device::State::Attached;
}

void USB_Device::dev_Status_In()
{
    dev()->onStatus_In();
}

void USB_Device::dev_Status_Out()
{

}

RESULT USB_Device::dev_Data_Setup( uint8_t RequestNo )
{
    Request     = USB_Request_Type;
    Recipient   = USB_Request_Recipient;

    return dev()->onDataSetup( RequestNo );
}

RESULT USB_Device::dev_NoData_Setup( uint8_t RequestNo )
{
    Request     = USB_Request_Type;
    Recipient   = USB_Request_Recipient;

    return dev()->onNoDataSetup( RequestNo );
}

RESULT USB_Device::dev_Get_Interface_Setting( uint8_t Interface, uint8_t AlternateSetting )
{
    if (AlternateSetting > 0)
    {
      return USB_UNSUPPORT;
    }
    else if (Interface > 1)
    {
      return USB_UNSUPPORT;
    }
    return USB_SUCCESS;
}

void USB_Device::dev_GetConfiguration()
{

}
void USB_Device::dev_SetConfiguration()
{
    DEVICE_INFO *pInfo = &Device_Info;

    if (pInfo->Current_Configuration != 0)
    {
      /* Device configured */
        dev()->devState = USB_Device::State::Configured;
    }
}

void USB_Device::dev_GetInterface()
{

}

void USB_Device::dev_SetInterface()
{

}

void USB_Device::dev_GetStatus()
{

}

void USB_Device::dev_ClearFeature()
{

}

void USB_Device::dev_SetEndPointFeature()
{

}

void USB_Device::dev_SetDeviceFeature()
{

}

void USB_Device::dev_SetDeviceAddress()
{
    dev()->devState = USB_Device::State::Addressed;
}

