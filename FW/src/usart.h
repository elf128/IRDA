/*
 *  usart.h
 *
 *  Created on: Jun 26, 2013
 *      Author: Denis aka caat
 */

#ifndef USART_H_
#define USART_H_

#include "Buffer.h"

extern unsigned int IrqCntUart4;

class UART
{

    uint8_t _tx[128];
    uint8_t _rx[128];
    ByteBuffer<uint8_t> txBuf;
    ByteBuffer<uint8_t> rxBuf;

public:
    UART() : txBuf( _tx, sizeof(_tx) ), rxBuf( _rx, sizeof(_rx) ) {};
    ~UART() {};

    static void     __init__();
    static void     UART_IRQHandler();
    static uint8_t  isThereNewData();
    static uint8_t  rx();
    static uint8_t  rxToBuffer( uint8_t* Buf, uint8_t maxLen );

    static uint8_t  tx( uint8_t data );
    static uint8_t  txFromBuffer( uint8_t* data, uint8_t len );

};

void UART_Init(void);
void UART_PutChar(uint8_t ch);
void UART_PutString(uint8_t *str);
void UART_PutCharDirect(uint8_t ch);
void UART_PutStringDirect(uint8_t *str);
void UART_Flush(void);

int UART_GetChar(void);
int UART_PeekChar(void);
int UART_Available(void);

void InitUartBuffer(void);
void TestUartBuffer(void);
extern "C"
void USART2_IRQHandler(void);

#endif /* USART_H_ */
