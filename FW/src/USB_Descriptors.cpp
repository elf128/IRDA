/*
 * USB_Descriptors.cpp
 *
 *  Created on: Jul 18, 2018
 *      Author: vlad
 */

#include "USB_Device.h"

extern "C"
{
    #include "usb_core.h"
}


uint8_t* USB_Device::dev_GetDeviceDescriptor( uint16_t Length )
{
    return Standard_GetDescriptorData(Length, desc );
}

uint8_t* USB_Device::dev_GetConfigDescriptor( uint16_t Length )
{
    return Standard_GetDescriptorData(Length, desc+1 );
}

uint8_t* USB_Device::dev_GetStringDescriptor( uint16_t Length )
{
    uint8_t wValue0 = Device_Info.USBwValue0;

    if (wValue0 > 4)
    {
        return 0;
    }
    else
    {
        return Standard_GetDescriptorData(Length, desc + wValue0 + 2 );
    }
}


void USB_Device::Endpoint::epSetType(   const USB_Device::Endpoint::Num EP, const EP_Type_e type )
{ setEPType( (uint8_t) EP, type );}

EP_Type_e USB_Device::Endpoint::epGetType(   const USB_Device::Endpoint::Num EP )
{ return getEPType( (uint8_t) EP ); }

void USB_Device::Endpoint::epSetRxStatus(   const USB_Device::Endpoint::Num EP, const EP_Status_e rxState )
{ setEPRxStatus( (uint8_t) EP, rxState );}

EP_Status_e USB_Device::Endpoint::epGetRxStatus(   const USB_Device::Endpoint::Num EP )
{ return getEPRxStatus( (uint8_t) EP ); }

void USB_Device::Endpoint::epSetTxStatus(   const USB_Device::Endpoint::Num EP, const EP_Status_e txState )
{ setEPTxStatus( (uint8_t) EP, txState );}

EP_Status_e USB_Device::Endpoint::epGetTxStatus(   const USB_Device::Endpoint::Num EP )
{ return getEPTxStatus( (uint8_t) EP ); }

void USB_Device::Endpoint::epSetStatus(   const USB_Device::Endpoint::Num EP, const EP_Status_e rxState, const EP_Status_e txState )
{ setEPRxTxStatus( (uint8_t) EP, rxState, txState ); }

void USB_Device::Endpoint::epSetTxAddr( const USB_Device::Endpoint::Num EP, const uint16_t Addr )
{ setEPTxAddr( (uint8_t)EP, Addr ); }

void USB_Device::Endpoint::epSetRxAddr( const USB_Device::Endpoint::Num EP, const uint16_t Addr )
{ setEPRxAddr( (uint8_t)EP, Addr ); }

uint16_t USB_Device::Endpoint::epGetTxCount(const USB_Device::Endpoint::Num EP )
{ return getEPTxCount( (uint8_t) EP ); }

uint16_t USB_Device::Endpoint::epGetRxCount(const USB_Device::Endpoint::Num EP )
{ return getEPRxCount( (uint8_t) EP ); }

void USB_Device::Endpoint::epSetTxCount(const USB_Device::Endpoint::Num EP, const uint16_t Num )
{ setEPTxCount( (uint8_t)EP, Num ); }

void USB_Device::Endpoint::epSetRxCount(const USB_Device::Endpoint::Num EP, const uint16_t Num )
{ setEPRxCount( (uint8_t)EP, Num ); }

void USB_Device::Endpoint::epSetOutStatus( const Endpoint::Num EP )
{ set_Status_Out( (uint8_t)EP );}

void USB_Device::Endpoint::epClrOutStatus( const Endpoint::Num EP )
{ clr_Status_Out( (uint8_t)EP );}

