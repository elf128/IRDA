/*
 *  utils.h
 *
 *  Created on: Jun 25, 2013
 *      Author: Denis caat
 */

#ifndef UTILS_H_
#define UTILS_H_

#define READ_LED_PIN        GPIO_Pin_9
#define READ_LED_PORT       GPIOB

#define WRITE_LED_PIN       GPIO_Pin_8
#define WRITE_LED_PORT      GPIOB

#define IRDA_SHUTDOWN_PIN   GPIO_Pin_4
#define IRDA_SHUTDOWN_PORT  GPIOA

void readLEDon(void);
void readLEDoff(void);
void readLEDtoggle(void);

void writeLEDon(void);
void writeLEDoff(void);
void writeLEDtoggle(void);

void Blink(void);

void Delay_ms(unsigned int ms);
void Delay_us(unsigned int us);

#endif /* UTILS_H_ */
