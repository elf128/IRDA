#include <stdint.h>

//#include "main.h"
//#include "adc.h"
//#include "comio.h"
//#include "commhandler.h"
//#include "config.h"
//#include "fasttrig.h"
//#include "engine.h"
//#include "gyro.h"
//#include "pwm.h"
//#include "pins.h"
//#include "rc.h"
extern "C"
{
    #include "utils.h"
//    #include "usb.h"
//    #include "hw_config.h"
    #include "stm32f10x_tim.h"
    #include "systick.h"
}
#include "usart.h"
#include "VCP_ftdi.h"

//VCP_FTDI device;

void Periph_clock_enable(void)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
//                           RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD |
//                           RCC_APB2Periph_GPIOE | RCC_APB2Periph_AFIO |
//                           RCC_APB2Periph_ADC1  | RCC_APB2Periph_TIM1 |
                           RCC_APB2Periph_TIM8, ENABLE);

    RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART2, ENABLE );//  | RCC_APB1Periph_TIM2 |
//                           RCC_APB1Periph_UART4 | RCC_APB1Periph_TIM3 |
//                           RCC_APB1Periph_TIM4, ENABLE);

    RCC_APB1PeriphResetCmd(RCC_APB1Periph_WWDG, DISABLE);

//    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1,  ENABLE);
}

void GPIO_Config(void)  //Configures GPIO
{
    GPIO_InitTypeDef    GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = READ_LED_PIN;           //LED1 Output Config
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(READ_LED_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = WRITE_LED_PIN;          //LED2 Output Config
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(WRITE_LED_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = IRDA_SHUTDOWN_PIN;      //IRDA Output Config
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(IRDA_SHUTDOWN_PORT, &GPIO_InitStructure);
}

void setup(void)
{
    InitSysTick();

    Periph_clock_enable();

    GPIO_Config();

    __enable_irq();

    UART_Init();
    //setupUSB();

    for (int i = 0; i < 4; i++)
    {
    	readLEDtoggle();
    	writeLEDtoggle();
        Delay_ms(50); //short blink
    }

    writeLEDon();
    VCP_FTDI::__init__()->usbInit();


    /*if (GetVCPConnectMode() != eVCPConnectReset)
    {
        Delay_ms(3000);

        if (GetVCPConnectMode() == eVCPConnectData)
        {
        }
    }
    else
    {
        Delay_ms(3000);
    }

	uint8_t c;
    while( usbBytesAvailable() )
    	usbReceiveBytes(&c, 1);
*/
    //SysTickAttachCallback(WatchDog);
    writeLEDoff();

    GPIO_ResetBits(IRDA_SHUTDOWN_PORT, IRDA_SHUTDOWN_PIN);

}

int main(void)
{

    setup();

    int idleLoops = 0;
//	uint8_t buf[16];
    unsigned int lastTime = micros();

    WWDG->SR = (uint32_t)RESET;

    while (1)
    {
        idleLoops++;
        unsigned int currentTime = micros();
        unsigned int timePassed = currentTime - lastTime;

        if( VCP_FTDI::dev()->receiveBuffer.isEmpty() )
        {
            writeLEDoff();
            VCP_FTDI::dev()->checkLockedData();
        }
        else
        {
            writeLEDon();
            uint8_t C;
            while ( VCP_FTDI::dev()->receiveBuffer.isEmpty() == 0 )
            {
                VCP_FTDI::dev()->receiveBuffer.Pop( C );
                UART_PutChar( C );
            }
            //VCP_FTDI::dev()->waitingBuffer.Push( C );
            //VCP_FTDI::dev()->waitingBuffer.Push( '-' );
            //VCP_FTDI::dev()->pushDataOut();
        }

        if ( UART_Available() )
        {
            while( UART_Available() )
            {
                readLEDon();
                uint8_t C = UART_GetChar();
        //                  int C = 0x31;
                VCP_FTDI::dev()->waitingBuffer.Push( C );
            }
        }
        else
        {
            VCP_FTDI::dev()->pushDataOut();
            readLEDoff();
        }

        if (timePassed >= 3000)
        {
            lastTime = currentTime;
            //UART_PutChar( 0x05 );
            WWDG->SR = (uint32_t)RESET;
        }

    }
}

extern "C" void WWDG_IRQHandler(void)
{

}

extern "C" void _init(void)
{
}

