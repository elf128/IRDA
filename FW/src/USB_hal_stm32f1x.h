/*
 * USB_hal_stm32f1x.h
 *
 *  Created on: Sep 11, 2018
 *      Author: vlad
 */

#ifndef SRC_USB_HAL_STM32F1X_H_
#define SRC_USB_HAL_STM32F1X_H_

#include <stdint.h>

enum class EP_Status_e
{
    Disabled = 0,
    Stall    = 1,
    NAK      = 2,
    Valid    = 3
};

enum class EP_Type_e
{
    Bulk = 0,
    Ctrl = 1,
    Iso  = 2,
    Irq  = 3
};

class USB_hal_stm32f1x
{
public:
    union USB_EP_Reg_t
    {
        struct {
            uint8_t     EA      : 4; // RW
            EP_Status_e StatTX  : 2; //   T
            uint8_t     DTOG_TX : 1; //   T
            uint8_t     CtrTX   : 1; // RC_W0
            uint8_t     EP_Kind : 1; // RW
            EP_Type_e   EP_Type : 2; // RW
            uint8_t     Setup   : 1; // R
            EP_Status_e StatRX  : 2; //   T
            uint8_t     DTOG_RX : 1; //   T
            uint8_t     CtrRX   : 1; // RC_W0
        } field;
        uint16_t raw[2];
    };

    struct USB_Regs_t
    {
        uint32_t CNTR;
        uint32_t ISTR;
        uint32_t FNR;
        uint32_t DADDR;
        uint32_t BTABLE;
    };

    struct USB_EP_t
    {
        uint32_t AddrTX;
        uint32_t CountTX;
        uint32_t AddrRX;
        uint32_t CountRX;
    };

    USB_hal_stm32f1x()  {}
    ~USB_hal_stm32f1x() {}
};

void        setBTable(       const uint16_t value );

void        setEPType(       const uint8_t EpNum, const EP_Type_e Type );
EP_Type_e   getEPType(       const uint8_t EpNum );
void        setEPRxStatus(   const uint8_t EpNum, const EP_Status_e State );
void        setEPTxStatus(   const uint8_t EpNum, const EP_Status_e State );
void        setEPRxTxStatus( const uint8_t EpNum, const EP_Status_e rxState, const EP_Status_e txState );

EP_Status_e getEPRxStatus(   const uint8_t EpNum );
EP_Status_e getEPTxStatus(   const uint8_t EpNum );

void setEPTxAddr( const uint8_t EpNum, const uint16_t Addr );
void setEPRxAddr( const uint8_t EpNum, const uint16_t Addr );

uint16_t getEPTxCount(   const uint8_t EpNum );
uint16_t getEPRxCount(   const uint8_t EpNum );
void setEPTxCount( const uint8_t EpNum, const uint16_t count );
void setEPRxCount( const uint8_t EpNum, const uint16_t count );

void setEP_Kind( const uint8_t EpNum );
void clrEP_Kind( const uint8_t EpNum );

#define set_Status_Out(bEpNum)  setEP_Kind(bEpNum)
#define clr_Status_Out(bEpNum)  clrEP_Kind(bEpNum)

#define setEPDoubleBuff(bEpNum) setEP_Kind(bEpNum)
#define clrEPDoubleBuff(bEpNum) clrEP_Kind(bEpNum)

extern volatile USB_hal_stm32f1x::USB_Regs_t*   USB_Reg;
extern volatile USB_hal_stm32f1x::USB_EP_Reg_t* USB_EPnR;
extern volatile USB_hal_stm32f1x::USB_EP_t*     USB_BDT;

#endif /* SRC_USB_HAL_STM32F1X_H_ */
