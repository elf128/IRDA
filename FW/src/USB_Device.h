/*
 * USB_Device.h
 *
 *  Created on: Jul 13, 2018
 *      Author: vlad
 */

#ifndef SRC_USB_DEVICE_H_
#define SRC_USB_DEVICE_H_

#include <stdint.h>
#include "stm32f10x.h"
#include "USB_hal_stm32f1x.h"

extern "C"
{
    #include "usb_core.h"
}

#define IMR_MSK (CNTR_CTRM  | CNTR_WKUPM | CNTR_ERRM  | CNTR_SOFM | CNTR_RESETM )  // HJI

#define BTABLE_ADDRESS      (0x00)

/* EP0  */
/* rx/tx buffer base address */
#define ENDP0_RXADDR        (0x40)
#define ENDP0_TXADDR        (0x80)

/* EP1  */
/* tx buffer base address */
#define ENDP1_TXADDR        (0xC0)
#define ENDP2_RXADDR        (0x100)

/* Exported constants --------------------------------------------------------*/
/* Definition of "USBbmRequestType" */
#define REQUEST_TYPE_MASK 0x60  /* Mask to get request type */
#define RECIPIENT_MASK    0x1F  /* Mask to get recipient */

class USB_Device
{
//	bool fSuspendEnabled = TRUE;  /* true when suspend is possible */
//	uint32_t EP[8];

//	struct
//	{
//		RESUME_STATE eState;
//		uint8_t bESOFcnt;
//	}
//	ResumeS;

//	__IO uint32_t remotewakeupon=0;

protected:
    enum class RESUME_STATE
    {
        EXTERNAL,
        INTERNAL,
        LATER,
        WAIT,
        START,
        ON,
        OFF,
        ESOF
    };

    enum class RequestType
    {
        Std_Req    = 0x00,  /* Standard request */
        Class_Req  = 0x20,  /* Class request */
        Vendor_Req = 0x40,  /* Vendor request */
    };

    enum class RecipientType
    {
      Device = 0,     /* Recipient device */
      Interface,      /* Recipient interface */
      Endpoint,       /* Recipient endpoint */
      Other
    };

    enum class StdRequestType
    {
      GET_STATUS = 0,
      CLEAR_FEATURE,
      RESERVED1,
      SET_FEATURE,
      RESERVED2,
      SET_ADDRESS,
      GET_DESCRIPTOR,
      SET_DESCRIPTOR,
      GET_CONFIGURATION,
      SET_CONFIGURATION,
      GET_INTERFACE,
      SET_INTERFACE,
      TOTAL_sREQUEST,  /* Total number of Standard request */
      SYNCH_FRAME = 12
    };

    /* Feature selector of a SET_FEATURE or CLEAR_FEATURE */
    enum class FeatureSelector
    {
      ENDPOINT_STALL,
      DEVICE_REMOTE_WAKEUP
    };

    static USB_Device*    _dev;
    static ONE_DESCRIPTOR  desc[6];
    static RequestType     Request;
    static RecipientType   Recipient;

    USB_Device() { // first access should happend somewhere where it's apropriate to initialise USB.
        _dev = this;
    }
    ~USB_Device() {}

public:
    enum class DescriptorType : uint8_t
    {
        Device        = 1,
        Configuration = 2,
        String        = 3,
        Interface     = 4,
        Endpoint      = 5,
        CS_Interface  = 0x24
    };

    enum class State
    {
        Unconnected   = 0,
        Attached,
        Powered,
        Suspended,
        Addressed,
        Configured,
    };

    class Endpoint
    {
    public:
        enum class Num : uint8_t
        {
            EP0 = 0,
            EP1 = 1,
            EP2 = 2,
            EP3 = 3,
            EP4 = 4,
            EP5 = 5,
            EP6 = 6,
            EP7 = 7,
        };

        Endpoint() {}
        ~Endpoint() {}

        static  void        epSetType(   const Endpoint::Num EP, const EP_Type_e type );
        static  EP_Type_e   epGetType(   const Endpoint::Num EP );

        static  void        epSetRxStatus( const USB_Device::Endpoint::Num EP, const EP_Status_e rxState );
        static  EP_Status_e epGetRxStatus( const USB_Device::Endpoint::Num EP );
        static  void        epSetTxStatus( const USB_Device::Endpoint::Num EP, const EP_Status_e txState );
        static  EP_Status_e epGetTxStatus( const USB_Device::Endpoint::Num EP );
        static  void        epSetStatus(   const USB_Device::Endpoint::Num EP, const EP_Status_e rxState, const EP_Status_e txState );

        static  void        epSetTxAddr( const Endpoint::Num EP, const uint16_t  Addr );
        static  void        epSetRxAddr( const Endpoint::Num EP, const uint16_t  Addr );
        static  uint16_t    epGetTxCount(const Endpoint::Num EP );
        static  uint16_t    epGetRxCount(const Endpoint::Num EP );
        static  void        epSetTxCount(const Endpoint::Num EP, const uint16_t  Num );
        static  void        epSetRxCount(const Endpoint::Num EP, const uint16_t  Num );

        static  void        epSetOutStatus( const Endpoint::Num EP );
        static  void        epClrOutStatus( const Endpoint::Num EP );
    };

    template <typename BODY>
    struct AbstractDescriptor
    {
        uint8_t                     bLength;
        USB_Device::DescriptorType  bDescriptorType;
        BODY                        body;
    }__attribute__((packed));

    struct DeviceDescriptorBody
    {
        uint16_t                    bcdUSB;
        uint8_t                     bDeviceClass;
        uint8_t                     bDeviceSubClass;
        uint8_t                     bDeviceProtocol;
        uint8_t                     bMaxPacketSize0;
        uint16_t                    idVendor;
        uint16_t                    idProduct;
        uint16_t                    bcdDevice;
        uint8_t                     bStrDescIdx[3];
        uint8_t                     bNumConfigurations;
    }__attribute__((packed));

    struct ConfigurationDescriptorBody
    {
        uint16_t                    wTotalLength;
        uint8_t                     bNumInterfaces;
        uint8_t                     bConfigurationValue;
        uint8_t                     iConfiguration;
        uint8_t                     bmAttributes;
        uint8_t                     MaxPower;
    }__attribute__((packed));

    struct InterfaceDescriptorBody
    {
        uint8_t                     bInterfaceNumber;
        uint8_t                     bAlternateSetting;
        uint8_t                     bNumEndpoints;
        uint8_t                     bInterfaceClass;
        uint8_t                     bInterfaceSubClass;
        uint8_t                     bInterfaceProtocol;
        uint8_t                     bStrIdxInterface;
    }__attribute__((packed));

    struct EndpointDescriptorBody
    {
        uint8_t                     bEndpointAddress;
        uint8_t                     bmAttributes;
        uint16_t                    wMaxPacketSize;
        uint8_t                     bInterval;
    }__attribute__((packed));

    typedef AbstractDescriptor<DeviceDescriptorBody>        DeviceDescriptor;
    typedef AbstractDescriptor<ConfigurationDescriptorBody> ConfigurationDescriptor;
    typedef AbstractDescriptor<InterfaceDescriptorBody>     InterfaceDescriptor;
    typedef AbstractDescriptor<EndpointDescriptorBody>      EndpointDescriptor;

    State     devState;

    static USB_Device* dev() { return _dev; }

    virtual void   usbInit();
    virtual void   epSetup() = 0;
    virtual RESULT onDataSetup(   uint8_t RequestNo ) {return USB_UNSUPPORT;}
    virtual RESULT onNoDataSetup( uint8_t RequestNo ) {return USB_UNSUPPORT;}
    virtual void   onStatus_In() = 0;

    // Following methods callbacks and they are required be USB_FS_Device_driver
    // All of them are called from usb_fs.
    static  void     dev_Init();
    static  void     dev_Reset();
    static  void     dev_Status_In();
    static  void     dev_Status_Out();
    static  RESULT   dev_Data_Setup(   uint8_t RequestNo );
    static  RESULT   dev_NoData_Setup( uint8_t RequestNo );
    static  RESULT   dev_Get_Interface_Setting( uint8_t Interface, uint8_t AlternateSetting );
    static  uint8_t* dev_GetDeviceDescriptor( uint16_t Length );
    static  uint8_t* dev_GetConfigDescriptor( uint16_t Length );
    static  uint8_t* dev_GetStringDescriptor( uint16_t Length );

    static  void     dev_GetConfiguration();
    static  void     dev_SetConfiguration();
    static  void     dev_GetInterface();
    static  void     dev_SetInterface();
    static  void     dev_GetStatus();
    static  void     dev_ClearFeature();
    static  void     dev_SetEndPointFeature();
    static  void     dev_SetDeviceFeature();
    static  void     dev_SetDeviceAddress();

    static  void __inline__ onNothing() {};
    static  void     onData();

    virtual void     SOF();
    virtual void     CTR();
    virtual void     RESET();
    virtual void     DOVR();
    virtual void     ERR();
    virtual void     WKUP();
    virtual void     SUSP();
    virtual void     ESOF();

//	virtual	void	Suspend();
//	virtual void	Resume_Init();
//	virtual	void 	Resume(	RESUME_STATE eResumeSetVal );
	virtual RESULT  PowerOn();
	virtual RESULT 	PowerOff();

    virtual void    pullSetup() = 0;
    virtual void    pullSet()   = 0;
    virtual void    pullClear() = 0;

private:
    void setupSystem();
    void setupClock();
    void setupIRQ();
};

extern void (*pEpInt_IN[7])(void);
extern void (*pEpInt_OUT[7])(void);

#define USB_Request_Recipient ( USB_Device::RecipientType( pInformation->USBbmRequestType &  RECIPIENT_MASK  ))
#define USB_Request_Type ( USB_Device::RequestType( pInformation->USBbmRequestType &  REQUEST_TYPE_MASK ))

#endif /* SRC_USB_DEVICE_H_ */
