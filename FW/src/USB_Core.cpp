/*
 * USB_Device.cpp
 *
 *  Created on: Jul 13, 2018
 *      Author: vlad
 */

#include "USB_Device.h"
#include "stm32_it.h"
extern "C"
{
	#include "usb_lib.h"
	#include "usb_init.h"
    #include "utils.h"   // To do: Remove this nonsence.
}
#include "stm32f10x_exti.h"
DEVICE Device_Table =
{
    4,
    1
};

DEVICE_PROP Device_Property =
{
	USB_Device::dev_Init,
	USB_Device::dev_Reset,
	USB_Device::dev_Status_In,
	USB_Device::dev_Status_Out,
	USB_Device::dev_Data_Setup,
	USB_Device::dev_NoData_Setup,
	USB_Device::dev_Get_Interface_Setting,
	USB_Device::dev_GetDeviceDescriptor,
	USB_Device::dev_GetConfigDescriptor,
	USB_Device::dev_GetStringDescriptor,
	0,
	0x40 /*MAX PACKET SIZE*/
};

USER_STANDARD_REQUESTS User_Standard_Requests =
{
	USB_Device::dev_GetConfiguration,
	USB_Device::dev_SetConfiguration,
	USB_Device::dev_GetInterface,
	USB_Device::dev_SetInterface,
	USB_Device::dev_GetStatus,
	USB_Device::dev_ClearFeature,
	USB_Device::dev_SetEndPointFeature,
	USB_Device::dev_SetDeviceFeature,
	USB_Device::dev_SetDeviceAddress
};

volatile uint16_t wIstr;  /* ISTR register last read value */
volatile uint8_t bIntPackSOF = 0;  /* SOFs received between 2 consecutive packets */
volatile uint32_t esof_counter =0; /* expected SOF counter */
volatile uint32_t wCNTR=0;

extern "C" void USB_Istr(void)
{
	USB_Device::onData();
}


void (*pEpInt_IN[7])(void) =
{
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
};

void (*pEpInt_OUT[7])(void) =
{
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
	USB_Device::onNothing,
};


void USB_Device::usbInit()
{
    // In order to work properly, device has to overload some EndPoint's handlers.
    // This should happend somewhere here.
    pullSetup();
    ///Pull down to create USB Disconnect Pulse
    pullClear();

    setupSystem();
    Delay_ms(200);
    //pullSet();

    setupClock();
    setupIRQ();
    USB_Init();
}

void USB_Device::setupSystem()
{
    // If any additional GPIO config has to be done. It's time to do it right here.
    EXTI_InitTypeDef EXTI_InitStructure;

    /* Configure the EXTI line 18 connected internally to the USB IP */
    EXTI_ClearITPendingBit(EXTI_Line18);
    EXTI_InitStructure.EXTI_Line = EXTI_Line18;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}

void USB_Device::setupClock()
{
    /* Select USBCLK source */
    RCC_USBCLKConfig( RCC_USBCLKSource_PLLCLK_Div1 );

    /* Enable the USB clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, ENABLE);
}

void USB_Device::setupIRQ()
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* 2 bit for pre-emption priority, 2 bits for subpriority */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

    /* Enable the USB interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* Enable the USB Wake-up interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USBWakeUp_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_Init(&NVIC_InitStructure);
}

void USB_Device::onData()
{
	wIstr = _GetISTR();

	if (wIstr & ISTR_SOF & wInterrupt_Mask)
		dev()->SOF();

    if (wIstr & ISTR_CTR & wInterrupt_Mask)
        dev()->CTR();

    if (wIstr & ISTR_RESET & wInterrupt_Mask)
        dev()->RESET();

//	if (wIstr & ISTR_DOVR & wInterrupt_Mask)
//	  _dev->DOVR();

	if (wIstr & ISTR_ERR & wInterrupt_Mask)
	    dev()->ERR();

	if (wIstr & ISTR_WKUP & wInterrupt_Mask)
	    dev()->WKUP();

//	if (wIstr & ISTR_SUSP & wInterrupt_Mask)
//	  _dev->SUSP();

//	if (wIstr & ISTR_ESOF & wInterrupt_Mask)
//	  _dev->ESOF();
}

void USB_Device::SOF()
{
    _SetISTR((uint16_t)CLR_SOF);
    bIntPackSOF++;
}

void USB_Device::CTR()
{
    /* servicing of the endpoint correct transfer interrupt */
    /* clear of the CTR flag into the sub */
    CTR_LP();
}

void USB_Device::RESET()
{
    _SetISTR((uint16_t)CLR_RESET);
    Device_Property.Reset();
}

void USB_Device::DOVR()
{
    _SetISTR((uint16_t)CLR_DOVR);
}

void USB_Device::ERR()
{
    _SetISTR((uint16_t)CLR_ERR);
}

void USB_Device::WKUP()
{
    _SetISTR((uint16_t)CLR_WKUP);
    //Resume(RESUME_STATE::EXTERNAL);
}

void USB_Device::SUSP()
{
	// check if SUSPEND is possible
/*
	if (fSuspendEnabled)
	{
	  Suspend();
	}
	else
	{
	  // if not possible then resume after xx ms
	  Resume(RESUME_STATE::LATER);
	}
	// clear of the ISTR bit must be done after setting of CNTR_FSUSP
*/
	_SetISTR((uint16_t)CLR_SUSP);
}

void USB_Device::ESOF()
{
	/* clear ESOF flag in ISTR */
	_SetISTR((uint16_t)CLR_ESOF);

/*	if ((_GetFNR()&FNR_RXDP)!=0)
	{
	  // increment ESOF counter
	  esof_counter ++;

	  // test if we enter in ESOF more than 3 times with FSUSP =0 and RXDP =1=>> possible missing SUSP flag
	  if ((esof_counter >3)&&((_GetCNTR()&CNTR_FSUSP)==0))
	  {
		// this a sequence to apply a force RESET

		//Store CNTR value
		wCNTR = _GetCNTR();

		// Store endpoints registers status
		for (i=0;i<8;i++) EP[i] = _GetENDPOINT(i);

		// apply FRES
		wCNTR|=CNTR_FRES;
		_SetCNTR(wCNTR);

		// clear FRES
		wCNTR&=~CNTR_FRES;
		_SetCNTR(wCNTR);

		// poll for RESET flag in ISTR
		while((_GetISTR()&ISTR_RESET) == 0);
		// clear RESET flag in ISTR
		_SetISTR((uint16_t)CLR_RESET);

	   // restore Enpoints
		for (i=0;i<8;i++)
		_SetENDPOINT(i, EP[i]);

		esof_counter = 0;
	  }
	}
	else
	{
		esof_counter = 0;
	}

	// resume handling timing is made with ESOFs
	Resume(RESUME_ESOF); // request without change of the machine state
	*/
}

RESULT USB_Device::PowerOn()
{
    uint16_t wRegVal;

    /*** cable plugged-in ? ***/
    pullSet();

    /*** CNTR_PWDN = 0 ***/
    wRegVal = CNTR_FRES;
    _SetCNTR(wRegVal);

    /*** CNTR_FRES = 0 ***/
    wInterrupt_Mask = 0;
    _SetCNTR(wInterrupt_Mask);
    /*** Clear pending interrupts ***/
    _SetISTR(0);
    /*** Set interrupt mask ***/
    wInterrupt_Mask = CNTR_RESETM | CNTR_SUSPM | CNTR_WKUPM;
    _SetCNTR(wInterrupt_Mask);

    return USB_SUCCESS;
}

RESULT USB_Device::PowerOff()
{
  /* disable all interrupts and force USB reset */
  _SetCNTR(CNTR_FRES);
  /* clear interrupt status register */
  _SetISTR(0);

  /* Disable the Pull-Up*/
  pullClear();
  /* switch-off device */
  _SetCNTR(CNTR_FRES + CNTR_PDWN);
  /* sw variables reset */
  /* ... */

  return USB_SUCCESS;
}

USB_Device*                 USB_Device::_dev = 0;
ONE_DESCRIPTOR              USB_Device::desc[6] = {{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
USB_Device::RequestType     USB_Device::Request;
USB_Device::RecipientType   USB_Device::Recipient;

