#================
#
# author: Vlad A,  elf128@gmail.com
#
#
#================

#==========
# First build?
# Edit here!!
#==========

TOOLCHAIN_PATH  = /home/vlad/usr/gcc-arm-none-eabi-5_4-2016q3/bin
TOOLCHAIN       = arm-none-eabi-

#==========

LD_SCRIPT = ldscripts/stm32_flash.ld 

INCLUDES  = src \
	lib/CMSIS/Include \
	lib/CMSIS/Device/ST/STM32F10x/Include \
	lib/STM32_USB-FS-Device_Driver/inc \
	lib/STM32F10x_StdPeriph_Driver/inc \
	lib/newlib 
#	src/VCP/inc \

SRC_DIRS  = src \
	src/startup \
	lib/STM32_USB-FS-Device_Driver/src
#	src/VCP/src \

LIBS = 

BUILD_DIR = build/tmp/
HEX_DIR = build/

#==========

TARGET = irda
DEBUG = 1
DEFINES = 

PREF            = @

CXX             = $(PREF)$(TOOLCHAIN_PATH)/$(TOOLCHAIN)g++
CC              = $(PREF)$(TOOLCHAIN_PATH)/$(TOOLCHAIN)gcc
AS              = $(PREF)$(TOOLCHAIN_PATH)/$(TOOLCHAIN)gcc -x assembler-with-cpp
OBJCOPY         = $(PREF)$(TOOLCHAIN_PATH)/$(TOOLCHAIN)objcopy
OBJDUMP         = $(PREF)$(TOOLCHAIN_PATH)/$(TOOLCHAIN)objdump
SIZE            = $(PREF)$(TOOLCHAIN_PATH)/$(TOOLCHAIN)size
RM              = rm -f

#==========

# define warning options here
CXX_WARNINGS = -Wall -Wextra -Wno-reorder
C_WARNINGS = -Wall -Wstrict-prototypes -Wextra

# C++ language standard ("c++98", "gnu++98" - default, "c++0x", "gnu++0x")
CXX_STD = gnu++0x

# C language standard ("c89" / "iso9899:1990", "iso9899:199409",
# "c99" / "iso9899:1999", "gnu89" - default, "gnu99")
C_STD = gnu11


#==========

C_DEFS    = -DSTM32F10X_MD -DHSE_VALUE=8000000 -DUSE_STDPERIPH_DRIVER
CXX_DEFS  = $(C_DEFS)

CORE_FLAGS= -mcpu=cortex-m3 -mthumb

OPTIMISATION = -O3

ifdef DEBUG
OPTIMISATION +=  -ggdb -g2
endif

CXX_FLAGS = -std=$(CXX_STD) -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -Wa,-ahlms=$(BUILD_DIR)$(notdir $(<:.cpp=.lst))
C_FLAGS = -std=$(C_STD) -Wa,-ahlms=$(BUILD_DIR)$(notdir $(<:.cpp=.lst))

LD_FLAGS = -nostartfiles -fno-exceptions $(patsubst %, -T%, $(LD_SCRIPT)) -Wl,-Map=$(HEX_DIR)$(TARGET).map,--cref,--no-warn-mismatch
#LD_FLAGS = $(patsubst %, -T%, $(LD_SCRIPT)) -Wl,-Map=$(HEX_DIR)$(TARGET).map,--cref,--no-warn-mismatch

AS_FLAGS = -g -ggdb3 -Wa,-amhls=$(BUILD_DIR)$(notdir $(<:.$(AS_EXT)=.lst))

# wildcard for  source files (all files with c or cpp extension found in
# current folder and SRCS_DIRS folders will be compiled and linked)
CXX_SRCS = $(wildcard $(patsubst %, %/*.cpp, . $(SRC_DIRS)))
C_SRCS = $(wildcard $(patsubst %, %/*.c, . $(SRC_DIRS)))

AS_SRCS = $(wildcard $(patsubst %, %/*.S, . $(SRC_DIRS)))

C_SRCS += lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_exti.c \
	lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_gpio.c \
	lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_usart.c \
	lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_rcc.c \
	lib/STM32F10x_StdPeriph_Driver/src/misc.c \
	lib/newlib/_syscalls.c\
	lib/newlib/_sbrk.c \
	lib/newlib/_exit.c \
	lib/newlib/_cxx.c \
#	system/src/cmsis/startup.c \

CXX_OBJS = $(addprefix $(BUILD_DIR), $(notdir $(CXX_SRCS:.cpp=.o)))
C_OBJS = $(addprefix $(BUILD_DIR), $(notdir $(C_SRCS:.c=.o)))
FONT_OBJS = $(addprefix $(BUILD_DIR), $(notdir $(MONO_FONTS:.font1_bin=.o)))
FONT_OBJS += $(addprefix $(BUILD_DIR), $(notdir $(MONO_A_FONTS:.font11_bin=.o)))
FONT_BINS = $(addprefix $(BUILD_DIR), $(MONO_FONTS))
FONT_BINS += $(addprefix $(BUILD_DIR), $(MONO_A_FONTS))

AS_OBJS = $(addprefix $(BUILD_DIR), $(notdir $(AS_SRCS:.S=.o)))

OBJS = $(AS_OBJS) $(C_OBJS) $(CXX_OBJS) $(FONT_OBJS)
DEPS = $(OBJS:.o=.d)

INC_DIRS_F = -I. $(patsubst %, -I%, $(INCLUDES))
LIB_DIRS_F = $(patsubst %, -L%, $(LIBS))

ELF = $(HEX_DIR)$(TARGET).elf
HEX = $(HEX_DIR)$(TARGET).hex
BIN = $(HEX_DIR)$(TARGET).bin
LSS = $(HEX_DIR)$(TARGET).lss
DMP = $(HEX_DIR)$(TARGET).dmp

# format final flags for tools, request dependancies for C++, C and asm
CXX_FLAGS_F = $(CORE_FLAGS) $(OPTIMISATION) $(CXX_WARNINGS) $(CXX_FLAGS)  $(CXX_DEFS) -MD -MP -MF $(BUILD_DIR)$(@F:.o=.d) $(INC_DIRS_F)
C_FLAGS_F = $(CORE_FLAGS) $(OPTIMISATION) $(C_WARNINGS) $(C_FLAGS) $(C_DEFS) -MD -MP -MF $(BUILD_DIR)$(@F:.o=.d) $(INC_DIRS_F)
#AS_FLAGS_F = $(CORE_FLAGS) $(AS_FLAGS) $(AS_DEFS) -MD -MP -MF $(BUILD_DIR)$(@F:.o=.d) $(INC_DIRS_F)
LD_FLAGS_F = $(CORE_FLAGS) $(LD_FLAGS) $(LIB_DIRS_F)

VPATH  = $(SRC_DIRS)
VPATH += $(BUILD_DIR)
VPATH += $(HEX_DIR)
VPATH += lib/STM32F10x_StdPeriph_Driver/src
VPATH += lib/newlib

#-------------------------------------------------------

debug:
	$(warning OBJS = $(OBJS))


all: hex

hex: make_output_dir $(ELF) $(LSS) $(HEX) $(BIN) print_size

bin: make_output_dir $(ELF) $(LSS) $(BIN) print_size

#$(ELF) : $(LD_SCRIPT)

$(ELF) : $(FONT_BINS) $(OBJS) $(LD_SCRIPT)
	@echo 'Link: $(ELF)'
	$(CXX) $(LD_FLAGS_F) $(OBJS) $(LIBS) -o $@
	@echo '-'

$(BUILD_DIR)%.o : %.cpp
	@echo 'Compile: $<'
	$(CXX) -c $(CXX_FLAGS_F) $< -o $@

$(BUILD_DIR)%.o : %.c
	@echo 'Compile: $<'
	$(CC) -c $(C_FLAGS_F) $< -o $@

$(BUILD_DIR)%.o : %.S
	@echo 'Compile asm: $<'
	$(AS) -c $(AS_FLAGS_F) $< -o $@

$(HEX) : $(ELF)
	@echo 'Creating IHEX image: $(HEX)'
	$(OBJCOPY) -O ihex $< $@
	@echo ' '

$(BIN) : $(ELF)
	@echo 'Binary image: $(BIN)'
	$(OBJCOPY) -O binary $< $@
	@echo ' '

$(LSS) : $(ELF)
	@echo 'Extended listing: $(LSS)'
	$(OBJDUMP) -S $< > $@
	@echo ' '

print_size: $(OBJS) $(ELF)
	@echo 'Size of modules:'
	$(SIZE) -B -t --common $(OBJS) $(USER_OBJS)
	@echo ' '
	@echo 'Size of target .elf file:'
	$(SIZE) -B $(ELF)
	@echo ' '
	@echo 'Grand total in .bin file:'
	@ls -l $(BIN)
	@echo ' '

#$(BUILD_DIR)%.font1_bin:
#	@echo 'Baking mono font: $@'
#	$(FONTBAKE) $(FONT_NAME) 16 24 20 1 ./font/additional $@

#$(BUILD_DIR)%.font11_bin:
#	@echo 'Baking mono+alpha font: $@'
#	$(FONTBAKE) $(FONT_NAME) 16 24 20 3 ./font/additional $@

#$(BUILD_DIR)%.font2_bin:
#	@echo 'Baking font: $@'
#	$(FONTBAKE) $(FONT_NAME) 16 24 20 2 ./font/additional $@

#$(BUILD_DIR)%.font22_bin:
#	@echo 'Baking font: $@'
#	$(FONTBAKE) $(FONT_NAME) 16 24 20 4 ./font/additional $@

#$(BUILD_DIR)%.o : %.font1_bin
#	@echo 'Wraping: $<'
#	$(OBJCOPY) -I binary -O elf32-littlearm -B arm --rename-section .data=.font --add-symbol $(notdir $(<:.font1_bin=))=.font:Mem $< $@

#$(BUILD_DIR)%.o : %.font11_bin
#	@echo 'Wraping: $<'
#	$(OBJCOPY) -I binary -O elf32-littlearm -B arm --rename-section .data=.font --add-symbol $(notdir $(<:.font11_bin=))=.font:Mem $< $@


#-----------------------------------------------------------------------------#
# create the desired output directory
#-----------------------------------------------------------------------------#

make_output_dir :
	@echo 'Setup dirs.'
	@mkdir -p $(strip $(HEX_DIR))
	@mkdir -p $(strip $(BUILD_DIR))

#=============================================================================#
# make clean
#=============================================================================#

clean:
ifeq ($(strip $(BUILD_DIR)), )
	@echo 'Removing all generated output files'
else
	@echo 'Removing all generated output files from output directory: $(BUILD_DIR)'
endif
	$(RM) $(BUILD_DIR)*
	
#ifneq ($(strip $(GENERATED)), )
#	$(RM) $(GENERATED)
#else
#	@echo 'Nothing to remove...'
#endif

