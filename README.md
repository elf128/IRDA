    Started on: Mar 31, 2018
    Author:     Vlad A <elf128@gmail.com>
       
This project is created to enable communication with dive computer Cressy Leonardo,
However, the technology used for comunication is quite flexible and, 
with minor modifications or in some cases, with no modifications, can be applied
for other devices. Including, but not exluding other dive computers.

Log:

* [0.2b](tags/0.2b) - Works with Android.
    - [irda_0.2b.bin](/uploads/89d85784ba543d576d90c9062ae8d956/irda.bin)
    - [irda_0.2b.hex](/uploads/c403403b449060130490dff50e95906d/irda.hex)
    - [irda_0.2b.elf](/uploads/dd9a827c2b81dfb0b183c1f13363b370/irda.elf)

---
* [0.2a](tags/0.2a) - FTDI'ish device.
    - [irda_0.2a.bin](/uploads/650285a575c96bda2bfac71ce63ad549/irda.bin)
    - [irda_0.2a.elf](/uploads/efb37c9284bdf17d5a302eba192a06bd/irda.elf)
    - [irda_0.2a.hex](/uploads/6143e66ed79c96e2c8811fb7b7000e7c/irda.hex)

---
* [0.1b](tags/0.1b) - legacy code with HW.0.2 support
    - [irda.0.1b.bin](/uploads/4120048157bafa77667dd78c553e46ec/irda.bin)
    - [irda.0.1b.hex](/uploads/2dc8a80b90b567e913d56ac0996a0c82/irda.hex)
    - [irda.0.1b.elf](/uploads/f038e18e3f3d9fcc8e96c72dc4c37813/irda.elf)

---
* [0.1a](/tags/0.1a) — first working binary. 
    - [irda.bin](/uploads/5dbd7091ac23315994bdfb91c3429fd9/irda.bin)
    - [irda.hex](/uploads/9a11f419325627022a4bba7bddeb4664/irda.hex)
    - [irda.elf](/uploads/a2ca5eebcbf72f1c589849c99e3a5863/irda.elf)
